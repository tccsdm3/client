package br.edu.ifspsaocarlos.sdm.toolview.model.message;

import com.google.gson.annotations.Expose;

import br.edu.ifspsaocarlos.sdm.toolview.model.Client;
import br.edu.ifspsaocarlos.sdm.toolview.model.type.ServerMessageType;

public class ServerMessage extends MessageI{

	@Expose
	private ServerMessageType serverMessageType;

	@Expose
	private Client client;
	
	public ServerMessage() {}

	public ServerMessage(ServerMessageType serverMessageType, Client client) {
		this.serverMessageType = serverMessageType;
		this.client = client;
	}

	public ServerMessageType getServerMessageType() {
		return serverMessageType;
	}

	public void setServerMessageType(ServerMessageType serverMessageType) {
		this.serverMessageType = serverMessageType;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}
