package br.edu.ifspsaocarlos.sdm.toolview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import br.edu.ifspsaocarlos.sdm.toolview.adapter.ClientAdapter;
import br.edu.ifspsaocarlos.sdm.toolview.controller.ClientsController;
import br.edu.ifspsaocarlos.sdm.toolview.model.Client;
import br.edu.ifspsaocarlos.sdm.toolview.service.SocketService;

public class ClientsActivity extends AppCompatActivity {

    private ListView lvClients;

    private ClientAdapter clientAdapter;

    private DataUpdateReceiver dataUpdateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients);

        lvClients = findViewById(R.id.lvClients);

        clientAdapter = new ClientAdapter(this, ClientsController.getInstance().getClientList());
        lvClients.setAdapter(clientAdapter);

        lvClients.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Client client = clientAdapter.getItem(i);
                if (!client.isShow())
                    return;
                Intent intent = new Intent(ClientsActivity.this, CallActivity.class);
                intent.putExtra(CallActivity.CLIENT_PARAM, client);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilterNewMessage = new IntentFilter(SocketService.UPDATE_CLIENT_TASK);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null)
            unregisterReceiver(dataUpdateReceiver);
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateList();
        }
    }

    private void updateList(){
        clientAdapter.notifyDataSetChanged();
    }
}
