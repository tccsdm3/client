package br.edu.ifspsaocarlos.sdm.toolview.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.edu.ifspsaocarlos.sdm.toolview.R;
import br.edu.ifspsaocarlos.sdm.toolview.model.Client;

/**
 * Created by PedroGuilherme on 03/07/2017.
 */

public class ClientAdapter extends ArrayAdapter<Client> {

    private LayoutInflater layoutInflater;

    public ClientAdapter(Activity activity, List<Client> clientList) {
        super(activity, R.layout.client_item, clientList);
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        Client client = getItem(position);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.client_item, null);
            holder = new ViewHolder();
            holder.name = convertView.findViewById(R.id.tvName);
            holder.call = convertView.findViewById(R.id.ivCall);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();

        holder.name.setText(client.getName());
        holder.call.setVisibility(client.isShow()?View.VISIBLE:View.INVISIBLE);

        return convertView;
    }

    static class ViewHolder {
        public TextView name;
        public ImageView call;
    }

}