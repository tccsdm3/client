package br.edu.ifspsaocarlos.sdm.toolview;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import br.edu.ifspsaocarlos.sdm.toolview.model.Client;
import br.edu.ifspsaocarlos.sdm.toolview.model.type.ServerMessageType;
import br.edu.ifspsaocarlos.sdm.toolview.service.SocketService;
import br.edu.ifspsaocarlos.sdm.toolview.util.Utils;

public class CallingActivity extends AppCompatActivity {

    public static final String CLIENT_PARAM = "CLIENT_PARAM";
    public static final int REQUEST_CAMERA = 0;
    public static final int REQUEST_AUDIO = 1;

    private DataUpdateReceiver dataUpdateReceiver;

    private TextView tvName;
    private ImageButton btnAccept;
    private ImageButton btnDeny;

    private Client clientCustomer;

    private boolean vibrate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);

        clientCustomer = (Client) getIntent().getSerializableExtra(CLIENT_PARAM);

        btnAccept = findViewById(R.id.btnAccept);
        btnDeny = findViewById(R.id.btnDeny);

        tvName = findViewById(R.id.tvName);

        if (clientCustomer==null)
            finish();

        tvName.setText(clientCustomer.getName()+getString(R.string.msg_is_calling));

        (new Thread(){
            @Override
            public void run() {
                while(vibrate){
                    Utils.vibrate(CallingActivity.this);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {e.printStackTrace();}
                }
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilterNewMessage = new IntentFilter(SocketService.CALL_TASK);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null)
            unregisterReceiver(dataUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        vibrate = false;
        super.onDestroy();
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SocketService.CALL_TASK)) {
                if (intent.hasExtra(SocketService.OUT_CALL_STATUS_PARAM)){
                    ServerMessageType serverMessageType = (ServerMessageType) intent.getSerializableExtra(SocketService.OUT_CALL_STATUS_PARAM);
                    switch (serverMessageType){
                        case USER_CANCEL_CALLING:
                            finish();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    public void accept(View view){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_AUDIO);
            else
                accept();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                        checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                    accept();
                else
                    requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_AUDIO);
            }
        }else if(requestCode == REQUEST_AUDIO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                        checkSelfPermission(Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED)
                    accept();
                else
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            }
        }
    }

    private void accept(){
        desableButtons();
        vibrate = false;

        acceptCall();

        Intent videoIntent = new Intent(this, VideoSupportActivity.class);
        videoIntent.putExtra(VideoSupportActivity.CLIENT_PARAM, clientCustomer);
        startActivity(videoIntent);
        finish();
    }

    public void deny(View view){
        desableButtons();

        denyCall();
        finish();
    }

    private void acceptCall(){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_ACCEPT_CALL_CLIENT_PARAM, clientCustomer);
        startService(sendSocketServiceIntent);
    }

    private void denyCall(){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_DENY_CALL_CLIENT_PARAM, clientCustomer);
        startService(sendSocketServiceIntent);
    }

    private void desableButtons(){
        btnAccept.setEnabled(false);
        btnDeny.setEnabled(false);
        btnAccept.setBackgroundColor(getResources().getColor(R.color.colorDisable));
        btnDeny.setBackgroundColor(getResources().getColor(R.color.colorDisable));
    }
}
