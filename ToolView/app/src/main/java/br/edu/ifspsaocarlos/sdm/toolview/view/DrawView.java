package br.edu.ifspsaocarlos.sdm.toolview.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import br.edu.ifspsaocarlos.sdm.toolview.R;

/**
 * Created by PedroGuilherme on 30/04/2018.
 */

public class DrawView extends View{

    private Path drawPath;
    private Paint drawPaint;

    private Canvas drawCanvas;
    private Bitmap canvasBitmap;

    private OnDrawEventListener eventListener;

    private boolean canDraw = true;

    public DrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        drawPath = new Path();
        drawPaint = new Paint();

        drawPaint.setColor(context.getResources().getColor(R.color.colorAccent));
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(12f);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(drawPath, drawPaint);
    }

    public void clear(){
        drawCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        drawPath.reset();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!canDraw)
            return false;

        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(x, y);
                break;
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPath(drawPath, drawPaint);
                if (eventListener!=null)
                    eventListener.onEvent(canvasBitmap);
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }

    public boolean isCanDraw() {
        return canDraw;
    }

    public void setCanDraw(boolean canDraw) {
        this.canDraw = canDraw;
    }

    public void setDrawEventListener(OnDrawEventListener eventListener) {
        this.eventListener = eventListener;
    }

    public interface OnDrawEventListener {
        void onEvent(Bitmap bitmap);
    }
}
