package br.edu.ifspsaocarlos.sdm.toolview.controller;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.edu.ifspsaocarlos.sdm.toolview.model.Client;

/**
 * Created by PedroGuilherme on 11/03/2018.
 */

public class ClientsController {

    private static ClientsController instance;

    private List<Client> clientList = new ArrayList<>();

    private ClientsController(){ }

    public static ClientsController getInstance(){
        if (instance == null)
            instance = new ClientsController();
        return instance;
    }

    public void addClient(Client client) {
        clientList.add(client);
    }

    public void changeClient(Client client) {
        clientList.get(clientList.indexOf(client)).setName(client.getName());
    }
    public void removeClient(Client client) {
        clientList.remove(client);
    }
    public void showClient(Client client, boolean value) {
        clientList.get(clientList.indexOf(client)).setShow(value);
    }

    public List<Client> getClientList() {
        return clientList;
    }
}
