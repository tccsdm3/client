package br.edu.ifspsaocarlos.sdm.toolview.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.edu.ifspsaocarlos.sdm.toolview.model.message.MessageI;

public class GsonSingleton {

	private static Gson gson;

	public static Gson getInstance(){
		if (gson == null){
			gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation()
					.registerTypeAdapter(MessageI.class, new MessageGsonAdapter())
					.create();
		}
		return gson;
	}
	
}
