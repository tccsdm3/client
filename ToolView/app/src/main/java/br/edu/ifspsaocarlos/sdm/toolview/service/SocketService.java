package br.edu.ifspsaocarlos.sdm.toolview.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

import br.edu.ifspsaocarlos.sdm.toolview.CallActivity;
import br.edu.ifspsaocarlos.sdm.toolview.CallingActivity;
import br.edu.ifspsaocarlos.sdm.toolview.controller.ClientsController;
import br.edu.ifspsaocarlos.sdm.toolview.model.Client;
import br.edu.ifspsaocarlos.sdm.toolview.model.message.Message;
import br.edu.ifspsaocarlos.sdm.toolview.model.message.MessageI;
import br.edu.ifspsaocarlos.sdm.toolview.model.message.ServerMessage;
import br.edu.ifspsaocarlos.sdm.toolview.model.type.ClientMessageType;
import br.edu.ifspsaocarlos.sdm.toolview.model.type.ServerMessageType;
import br.edu.ifspsaocarlos.sdm.toolview.util.GsonSingleton;
import br.edu.ifspsaocarlos.sdm.toolview.util.Utils;

/**
 * Created by PedroGuilherme on 18/02/2018.
 */

public class SocketService extends Service{

    public static final String CONNECTION_STATUS_TASK = "CONNECTION_STATUS_TASK";
    public static final String UPDATE_CLIENT_TASK = "UPDATE_CLIENT_TASK";
    public static final String CALL_TASK = "CALL_TASK";

    public static final String IN_NAME_PARAM = "IN_NAME_PARAM";
    public static final String IN_CALL_CLIENT_PARAM = "IN_CALL_CLIENT_PARAM";
    public static final String IN_CANCEL_CALL_CLIENT_PARAM = "IN_CANCEL_CALL_CLIENT_PARAM";
    public static final String IN_ACCEPT_CALL_CLIENT_PARAM = "IN_ACCEPT_CALL_CLIENT_PARAM";
    public static final String IN_DENY_CALL_CLIENT_PARAM = "IN_DENY_CALL_CLIENT_PARAM";
    public static final String IN_EXIT_CALL_CLIENT_PARAM = "IN_EXIT_CALL_CLIENT_PARAM";
    public static final String IN_DRAWING = "IN_DRAWING";
    public static final String IN_SEND_DRAW = "IN_SEND_DRAW";
    public static final String IN_SEND_CLEAR_DRAW = "IN_SEND_CLEAR_DRAW";

    public static final String OUT_CONNECTION_STATUS_PARAM = "OUT_CONNECTION_STATUS_PARAM";
    public static final String OUT_CALL_STATUS_PARAM = "OUT_CALL_STATUS_PARAM";
    public static final String OUT_DRAWING_PARAM = "OUT_DRAWING_PARAM";
    public static final String OUT_DREW_PARAM = "OUT_DREW_PARAM";
    public static final String OUT_CLEAR_DRAW_PARAM = "OUT_CLEAR_DRAW_PARAM";

    public static final String VIDEO_CAN_CONNECT = "VIDEO_CAN_CONNECT";
    public static final String VIDEO_OFFER = "VIDEO_OFFER";
    public static final String VIDEO_CANDIDATE = "VIDEO_CANDIDATE";
    public static final String VIDEO_ANSWER = "VIDEO_ANSWER";
    public static final String VIDEO_CREATEOFFER = "VIDEO_CREATEOFFER";

    private Socket socket;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        thread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.hasExtra(IN_NAME_PARAM))
            send(new Message(ClientMessageType.CHANGE_NAME, intent.getStringExtra(IN_NAME_PARAM)));
        else if (intent.hasExtra(IN_CALL_CLIENT_PARAM)) {
            Client client = (Client) intent.getSerializableExtra(IN_CALL_CLIENT_PARAM);
            send(new Message(ClientMessageType.CALL_USER, String.valueOf(client.getId())));
        }
        else if (intent.hasExtra(IN_CANCEL_CALL_CLIENT_PARAM)) {
            Client client = (Client) intent.getSerializableExtra(IN_CANCEL_CALL_CLIENT_PARAM);
            send(new Message(ClientMessageType.CANCEL_CALL_USER, String.valueOf(client.getId())));
        }
        else if (intent.hasExtra(IN_ACCEPT_CALL_CLIENT_PARAM)) {
            Client client = (Client) intent.getSerializableExtra(IN_ACCEPT_CALL_CLIENT_PARAM);
            send(new Message(ClientMessageType.ACCEPT_CALL, String.valueOf(client.getId())));
        }
        else if (intent.hasExtra(IN_DENY_CALL_CLIENT_PARAM)) {
            Client client = (Client) intent.getSerializableExtra(IN_DENY_CALL_CLIENT_PARAM);
            send(new Message(ClientMessageType.DENY_CALL, String.valueOf(client.getId())));
        }
        else if (intent.hasExtra(IN_DRAWING))
            send(new Message(ClientMessageType.DRAWING, String.valueOf(intent.getBooleanExtra(IN_DRAWING, false))));
        else if (intent.hasExtra(IN_SEND_DRAW))
            send(new Message(ClientMessageType.DREW, intent.getStringExtra(IN_SEND_DRAW)));
        else if (intent.hasExtra(IN_SEND_CLEAR_DRAW))
            send(new Message(ClientMessageType.CLEAR_DRAW, ""));
        else if (intent.hasExtra(IN_EXIT_CALL_CLIENT_PARAM))
            send(new Message(ClientMessageType.EXIT_CALL));
        else if (intent.hasExtra(VIDEO_OFFER))
            send(new Message(ClientMessageType.OFFER, intent.getStringExtra(VIDEO_OFFER)));
        else if (intent.hasExtra(VIDEO_CANDIDATE))
            send(new Message(ClientMessageType.CANDIDATE, intent.getStringExtra(VIDEO_CANDIDATE)));
        else if (intent.hasExtra(VIDEO_ANSWER))
            send(new Message(ClientMessageType.ANSWER, intent.getStringExtra(VIDEO_ANSWER)));
        else if (intent.hasExtra(VIDEO_CREATEOFFER))
            send(new Message(ClientMessageType.CREATEOFFER, intent.getStringExtra(VIDEO_CREATEOFFER)));
        else if (intent.hasExtra(VIDEO_CAN_CONNECT))
            send(new Message(ClientMessageType.CAN_CONNECT, intent.getStringExtra(VIDEO_CAN_CONNECT)));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (socket==null)
            return;
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        socket = null;
    }

    private Thread thread = new Thread(){
        @Override
        public void run() {
            try {
                socket = new Socket("192.168.0.106", 2018);
                notifyConnctionStatusToReceivers(true);

                Scanner scanner = new Scanner(socket.getInputStream());
                while (scanner.hasNextLine()) {
                    String aux = scanner.nextLine();
                    MessageI messageI = GsonSingleton.getInstance().fromJson(aux, MessageI.class);

                    if (messageI instanceof ServerMessage) {
                        ServerMessage serverMessage = (ServerMessage)messageI;
                        switch (serverMessage.getServerMessageType()) {
                            case ADD_USER:
                                ClientsController.getInstance().addClient(serverMessage.getClient());
                                notifyUpdateReceivers();
                                Log.e("DEBUG", "NOVO CLIENTE:" + serverMessage.getClient());
                                break;
                            case DEL_USER:
                                ClientsController.getInstance().removeClient(serverMessage.getClient());
                                notifyUpdateReceivers();
                                Log.e("DEBUG", "CLIENTE SAIU:" + serverMessage.getClient());
                                break;
                            case CHANGE_NAME:
                                ClientsController.getInstance().changeClient(serverMessage.getClient());
                                notifyUpdateReceivers();
                                Log.e("DEBUG", "CLIENTE MUDOU NOME:" + serverMessage.getClient());
                                break;
                            case USER_CALLING:
                                Intent dialogIntent = new Intent(SocketService.this, CallingActivity.class);
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                dialogIntent.putExtra(CallActivity.CLIENT_PARAM, serverMessage.getClient());
                                startActivity(dialogIntent);
                                Log.e("DEBUG", "CLIENTE LIGANDO:" + serverMessage.getClient());
                                break;
                            case USER_CANCEL_CALLING:
                                notifyCallReceivers(ServerMessageType.USER_CANCEL_CALLING);
                                Log.e("DEBUG", "CLIENTE CANCELOU A LIGACAO:" + serverMessage.getClient());
                                break;
                            case USER_ACCEPT:
                                notifyCallReceivers(ServerMessageType.USER_ACCEPT);
                                Log.e("DEBUG", "CLIENTE ACEITOU A LIGACAO:" + serverMessage.getClient());
                                break;
                            case USER_DENY:
                                notifyCallReceivers(ServerMessageType.USER_DENY);
                                Log.e("DEBUG", "CLIENTE NEGOU A LIGACAO:" + serverMessage.getClient());
                                break;
                            case HIDE_USER:
                                ClientsController.getInstance().showClient(serverMessage.getClient(), false);
                                notifyUpdateReceivers();
                                Log.e("DEBUG", "CLIENTE ESTA EM CHAMADA:" + serverMessage.getClient());
                                break;
                            case SHOW_USER:
                                ClientsController.getInstance().showClient(serverMessage.getClient(), true);
                                notifyUpdateReceivers();
                                Log.e("DEBUG", "CLIENTE ESTA DISPONIVEL:" + serverMessage.getClient());
                                break;
                            default:
                                break;
                        }
                    }else if (messageI instanceof Message) {
                        Message message = (Message) messageI;
                        switch (message.getMessageType()) {
                            case DRAWING:
                                notifyDrawing(Boolean.valueOf(message.getValue()));
                                Log.e("DEBUG", "SUPORTE ESTA DESENHANDO:" + message.getValue());
                                break;
                            case DREW:
                                notifyDrew(message.getValue());
                                Log.e("DEBUG", "SUPORTE DESENHOU:" + message.getValue());
                                break;
                            case CLEAR_DRAW:
                                notifyClearDraw();
                                Log.e("DEBUG", "SUPORTE APAGOU O DESENHO:" + message.getValue());
                                break;
                            case CAN_CONNECT:
                                notifyVideoCallReceivers(VIDEO_CAN_CONNECT, message.getValue());
                                Log.e("DEBUG", "CAN_CONNECT:" + message.getValue());
                                break;
                            case OFFER:
                                notifyVideoCallReceivers(VIDEO_OFFER, message.getValue());
                                Log.e("DEBUG", "OFFER:" + message.getValue());
                                break;
                            case CANDIDATE:
                                notifyVideoCallReceivers(VIDEO_CANDIDATE, message.getValue());
                                Log.e("DEBUG", "CANDIDATE:" + message.getValue());
                                break;
                            case ANSWER:
                                notifyVideoCallReceivers(VIDEO_ANSWER, message.getValue());
                                Log.e("DEBUG", "ANSWER:" + message.getValue());
                                break;
                            case CREATEOFFER:
                                notifyVideoCallReceivers(VIDEO_CREATEOFFER, message.getValue());
                                Log.e("DEBUG", "CREATEOFFER:" + message.getValue());
                                break;
                            default:
                                break;
                        }
                    }
                }
            } catch (IOException e) {
                notifyConnctionStatusToReceivers(false);
                e.printStackTrace();
            }
            Log.e("DEBUG","ACABOU A THREAD");
        }
    };

    public void send(final Message message){
        if (socket == null)
            return;
        (new Thread(){
            @Override
            public void run() {
                PrintStream printStream;
                try {
                    printStream = new PrintStream(socket.getOutputStream());
                    printStream.print(GsonSingleton.getInstance().toJson(message, MessageI.class)+System.lineSeparator());
                    Log.e("ENVIA",GsonSingleton.getInstance().toJson(message, MessageI.class)+System.lineSeparator());
                } catch (IOException e) {e.printStackTrace();}
            }
        }).start();
    }

    private void notifyUpdateReceivers(){
        Intent intent = new Intent(UPDATE_CLIENT_TASK);
        sendBroadcast(intent);
    }

    private void notifyConnctionStatusToReceivers(boolean status){
        Intent intent = new Intent(CONNECTION_STATUS_TASK);
        intent.putExtra(OUT_CONNECTION_STATUS_PARAM, status);
        sendBroadcast(intent);
    }

    private void notifyCallReceivers(ServerMessageType serverMessageType){
        Intent intent = new Intent(CALL_TASK);
        intent.putExtra(OUT_CALL_STATUS_PARAM, serverMessageType);
        sendBroadcast(intent);
    }

    private void notifyDrawing(boolean status){
        Intent intent = new Intent(CALL_TASK);
        intent.putExtra(OUT_DRAWING_PARAM, status);
        sendBroadcast(intent);
    }

    private void notifyDrew(String image){
        Intent intent = new Intent(CALL_TASK);
        intent.putExtra(OUT_DREW_PARAM, image);
        sendBroadcast(intent);
    }

    private void notifyClearDraw(){
        Intent intent = new Intent(CALL_TASK);
        intent.putExtra(OUT_CLEAR_DRAW_PARAM, true);
        sendBroadcast(intent);
    }

    private void notifyVideoCallReceivers(String param, String value){
        Intent intent = new Intent(CALL_TASK);
        intent.putExtra(param, value);
        sendBroadcast(intent);
    }

}