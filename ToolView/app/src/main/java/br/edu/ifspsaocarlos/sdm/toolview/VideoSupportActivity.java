package br.edu.ifspsaocarlos.sdm.toolview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.opengl.GLSurfaceView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.ArrayList;

import br.edu.ifspsaocarlos.sdm.toolview.controller.ClientsController;
import br.edu.ifspsaocarlos.sdm.toolview.model.Client;
import br.edu.ifspsaocarlos.sdm.toolview.service.SocketService;
import br.edu.ifspsaocarlos.sdm.toolview.util.Utils;
import br.edu.ifspsaocarlos.sdm.toolview.view.DrawView;

public class VideoSupportActivity extends AppCompatActivity {

    public static final String CLIENT_PARAM = "CLIENT_PARAM";
    private static final String VIDEO_TRACK_ID = "video_track";
    private static final String AUDIO_TRACK_ID = "audio_track";
    private static final String LOCAL_STREAM_ID = "local_stream";
    private static final String SDP = "sdp";
    private static final String SDP_MID = "sdpMid";
    private static final String SDP_M_LINE_INDEX = "sdpMLineIndex";

    private DataUpdateReceiver dataUpdateReceiver;

    private DrawView drawView;
    private FloatingActionButton fabDraw;
    private FloatingActionButton fabDeleteDraws;
    private GLSurfaceView videoView;

    private Client clientCustomer;

    private PeerConnectionFactory peerConnectionFactory;
    private MediaStream localMediaStream;
    private PeerConnection peerConnection;
    private VideoRenderer otherPeerRenderer;

    private PeerConnectionObserver peerConnectionObserver;
    private MySdpObserver mySdpObserver;

    private VideoSource localVideoSource;

    private boolean videoRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_support);

        clientCustomer = (Client) getIntent().getSerializableExtra(CLIENT_PARAM);
        if (clientCustomer==null)
            finish();

        drawView = findViewById(R.id.drawView);
        drawView.setCanDraw(false);
        drawView.setDrawEventListener(new DrawView.OnDrawEventListener() {
            @Override
            public void onEvent(Bitmap bitmap) {
                drawView.setCanDraw(false);
                fabDraw.setImageResource(R.drawable.ic_lead_pencil);
                fabDeleteDraws.setVisibility(View.VISIBLE);
                sendDraw(bitmap);
            }
        });

        fabDraw = findViewById(R.id.fabDraw);
        fabDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!drawView.isCanDraw()){
                    sendDrawing(true);
                    drawView.setCanDraw(true);
                    fabDraw.setImageResource(R.drawable.ic_close_circle);
                }else{
                    sendDrawing(false);
                    drawView.setCanDraw(false);
                    fabDraw.setImageResource(R.drawable.ic_lead_pencil);
                }

            }
        });

        fabDeleteDraws = findViewById(R.id.fabDeleteDraws);
        fabDeleteDraws.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.clear();
                fabDeleteDraws.setVisibility(View.GONE);
                sendClearDraw();
            }
        });

        videoView = findViewById(R.id.videoView);
        VideoRendererGui.setView(videoView, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilterNewMessage = new IntentFilter(SocketService.CALL_TASK);
        IntentFilter intentFilterNewMessage2 = new IntentFilter(SocketService.UPDATE_CLIENT_TASK);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage2);

        if (videoRunning)
            return;
        videoRunning = true;
        initStream();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null)
            unregisterReceiver(dataUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.localVideoSource != null)
            this.localVideoSource.stop();
        peerConnection.close();
        peerConnection.dispose();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_EXIT_CALL_CLIENT_PARAM, "");
        startService(sendSocketServiceIntent);
    }

    private void sendDrawing(boolean value){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_DRAWING, value);
        startService(sendSocketServiceIntent);
    }

    private void sendDraw(Bitmap bitmap){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_SEND_DRAW, Utils.getStringFromBitmap(bitmap));
        startService(sendSocketServiceIntent);
    }

    private void sendClearDraw(){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_SEND_CLEAR_DRAW, "");
        startService(sendSocketServiceIntent);
    }

    private void sendWEBRTCMessage(String messageType, String value){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(messageType, value);
        startService(sendSocketServiceIntent);
    }

    private void initStream() {
        PeerConnectionFactory.initializeAndroidGlobals(
                this,  // Context
                true,  // Audio Enabled
                true,  // Video Enabled
                true,  // Hardware Acceleration Enabled
                null); // Render EGL Context

        peerConnectionFactory = new PeerConnectionFactory();

        VideoCapturerAndroid vc = VideoCapturerAndroid.create(VideoCapturerAndroid.getNameOfBackFacingDevice(), null);
        localVideoSource = peerConnectionFactory.createVideoSource(vc, new MediaConstraints());
        VideoTrack localVideoTrack = peerConnectionFactory.createVideoTrack(VIDEO_TRACK_ID, localVideoSource);
        localVideoTrack.setEnabled(false);

        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        audioManager.setSpeakerphoneOn(true);
        AudioSource audioSource = peerConnectionFactory.createAudioSource(new MediaConstraints());
        AudioTrack localAudioTrack = peerConnectionFactory.createAudioTrack(AUDIO_TRACK_ID, audioSource);
        localAudioTrack.setEnabled(true);

        localMediaStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID);
        localMediaStream.addTrack(localVideoTrack);
        localMediaStream.addTrack(localAudioTrack);

        try {
            otherPeerRenderer = VideoRendererGui.createGui(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, false);
            connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        peerConnectionObserver = new PeerConnectionObserver();
        mySdpObserver = new MySdpObserver();

        ArrayList<PeerConnection.IceServer> iceServers = new ArrayList<>();

        peerConnection = peerConnectionFactory.createPeerConnection(
                iceServers,
                new MediaConstraints(),
                peerConnectionObserver);

        peerConnection.addStream(localMediaStream);

        sendWEBRTCMessage(SocketService.VIDEO_CAN_CONNECT, "");
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SocketService.CALL_TASK)) {
                if (intent.hasExtra(SocketService.VIDEO_CREATEOFFER))
                    peerConnection.createOffer(mySdpObserver, new MediaConstraints());
                else if (intent.hasExtra(SocketService.VIDEO_ANSWER)){
                    try {
                        JSONObject obj = new JSONObject(intent.getStringExtra(SocketService.VIDEO_ANSWER));
                        SessionDescription sdp = new SessionDescription(SessionDescription.Type.ANSWER,
                                obj.getString(SDP));
                        peerConnection.setRemoteDescription(mySdpObserver, sdp);
                    } catch (JSONException e) {e.printStackTrace();}
                }
                else if (intent.hasExtra(SocketService.VIDEO_CANDIDATE)){
                    try {
                        JSONObject obj = new JSONObject(intent.getStringExtra(SocketService.VIDEO_CANDIDATE));
                        peerConnection.addIceCandidate(
                                new IceCandidate(obj.getString(SDP_MID),
                                        obj.getInt(SDP_M_LINE_INDEX),
                                        obj.getString(SDP)));
                    }catch (Exception e){e.printStackTrace();}
                }
            }else if (intent.getAction().equals(SocketService.UPDATE_CLIENT_TASK)) {
                int index = ClientsController.getInstance().getClientList().indexOf(clientCustomer);
                if (index <0 || ClientsController.getInstance().getClientList().get(index).isShow())
                    finish();
            }
        }
    }

    private class MySdpObserver implements SdpObserver {
        @Override
        public void onCreateSuccess(SessionDescription sessionDescription) {
            peerConnection.setLocalDescription(mySdpObserver, sessionDescription);
            try {
                JSONObject obj = new JSONObject();
                obj.put(SDP, sessionDescription.description);
                sendWEBRTCMessage(SocketService.VIDEO_OFFER, obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSetSuccess() {}

        @Override
        public void onCreateFailure(String s) {}

        @Override
        public void onSetFailure(String s) {}
    }

    private class PeerConnectionObserver implements PeerConnection.Observer{

        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {}

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {}

        @Override
        public void onIceConnectionReceivingChange(boolean b) {}

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {}

        @Override
        public void onIceCandidate(IceCandidate iceCandidate) {
            try {
                JSONObject obj = new JSONObject();
                obj.put(SDP_MID, iceCandidate.sdpMid);
                obj.put(SDP_M_LINE_INDEX, iceCandidate.sdpMLineIndex);
                obj.put(SDP, iceCandidate.sdp);
                sendWEBRTCMessage(SocketService.VIDEO_CANDIDATE, obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onAddStream(MediaStream mediaStream) {
            mediaStream.videoTracks.getFirst().addRenderer(otherPeerRenderer);
        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {}

        @Override
        public void onDataChannel(DataChannel dataChannel) {}

        @Override
        public void onRenegotiationNeeded() {}
    }
}
