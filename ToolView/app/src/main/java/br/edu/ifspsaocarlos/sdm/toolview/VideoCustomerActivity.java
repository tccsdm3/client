package br.edu.ifspsaocarlos.sdm.toolview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.ArrayList;

import br.edu.ifspsaocarlos.sdm.toolview.controller.ClientsController;
import br.edu.ifspsaocarlos.sdm.toolview.model.Client;
import br.edu.ifspsaocarlos.sdm.toolview.service.SocketService;
import br.edu.ifspsaocarlos.sdm.toolview.util.Utils;

public class VideoCustomerActivity extends AppCompatActivity implements SensorEventListener {

    public static final String CLIENT_PARAM = "CLIENT_PARAM";
    private static final String VIDEO_TRACK_ID = "video_track";
    private static final String AUDIO_TRACK_ID = "audio_track";
    private static final String LOCAL_STREAM_ID = "local_stream";
    private static final String SDP = "sdp";
    private static final String SDP_MID = "sdpMid";
    private static final String SDP_M_LINE_INDEX = "sdpMLineIndex";

    private ImageView imageView;
    private GLSurfaceView videoView;
    private Button btnOtherClienteDrawing;

    private DataUpdateReceiver dataUpdateReceiver;

    private Client clientSupport;

    private PeerConnectionFactory peerConnectionFactory;
    private MediaStream localMediaStream;
    private PeerConnection peerConnection;

    private PeerConnectionObserver peerConnectionObserver;
    private MySdpObserver mySdpObserver;

    private VideoSource localVideoSource;

    private boolean videoRunning = false;
    private boolean supportIsDrawing = false;

    private SensorManager sensorManager;
    private Sensor sensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_customer);

        clientSupport = (Client) getIntent().getSerializableExtra(CLIENT_PARAM);
        if (clientSupport==null)
            finish();

        imageView = findViewById(R.id.imageView);

        videoView = findViewById(R.id.videoView);
        VideoRendererGui.setView(videoView, null);

        btnOtherClienteDrawing = findViewById(R.id.btnOtherClienteDrawing);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (sensor!=null)
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);

        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilterNewMessage = new IntentFilter(SocketService.CALL_TASK);
        IntentFilter intentFilterNewMessage2 = new IntentFilter(SocketService.UPDATE_CLIENT_TASK);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage2);

        if (videoRunning)
            return;
        videoRunning = true;
        initStream();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (sensor!=null)
            sensorManager.unregisterListener(this);
        if (dataUpdateReceiver != null)
            unregisterReceiver(dataUpdateReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_EXIT_CALL_CLIENT_PARAM, "");
        startService(sendSocketServiceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.localVideoSource != null)
            this.localVideoSource.stop();
        peerConnection.close();
        peerConnection.dispose();
    }

    private void sendWEBRTCMessage(String messageType, String value){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(messageType, value);
        startService(sendSocketServiceIntent);
    }

    private void initStream() {
        PeerConnectionFactory.initializeAndroidGlobals(
                this,  // Context
                true,  // Audio Enabled
                true,  // Video Enabled
                true,  // Hardware Acceleration Enabled
                null); // Render EGL Context

        peerConnectionFactory = new PeerConnectionFactory();

        VideoCapturerAndroid vc = VideoCapturerAndroid.create(VideoCapturerAndroid.getNameOfBackFacingDevice(), null);
        localVideoSource = peerConnectionFactory.createVideoSource(vc, new MediaConstraints());
        VideoTrack localVideoTrack = peerConnectionFactory.createVideoTrack(VIDEO_TRACK_ID, localVideoSource);
        localVideoTrack.setEnabled(true);

        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        audioManager.setSpeakerphoneOn(true);
        AudioSource audioSource = peerConnectionFactory.createAudioSource(new MediaConstraints());
        AudioTrack localAudioTrack = peerConnectionFactory.createAudioTrack(AUDIO_TRACK_ID, audioSource);
        localAudioTrack.setEnabled(true);

        localMediaStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID);
        localMediaStream.addTrack(localVideoTrack);
        localMediaStream.addTrack(localAudioTrack);

        try {
            VideoRenderer renderer = VideoRendererGui.createGui(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, false);
            localVideoTrack.addRenderer(renderer);

            connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        peerConnectionObserver = new PeerConnectionObserver();
        mySdpObserver = new MySdpObserver();

        ArrayList<PeerConnection.IceServer> iceServers = new ArrayList<>();

        peerConnection = peerConnectionFactory.createPeerConnection(
                iceServers,
                new MediaConstraints(),
                peerConnectionObserver);

        peerConnection.addStream(localMediaStream);
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SocketService.CALL_TASK)) {
                if (intent.hasExtra(SocketService.OUT_DREW_PARAM)) {
                    imageView.setY(0);
                    imageView.setX(0);
                    imageView.setImageBitmap(Utils.getBitmapFromString(intent.getStringExtra(SocketService.OUT_DREW_PARAM)));
                    btnOtherClienteDrawing.setVisibility(View.GONE);
                }
                else if (intent.hasExtra(SocketService.OUT_DRAWING_PARAM)) {
                    supportIsDrawing = intent.getBooleanExtra(SocketService.OUT_DRAWING_PARAM, false);
                    if (supportIsDrawing) {
                        btnOtherClienteDrawing.setText(String.format(getString(R.string.client_drawing), clientSupport.getName()));
                        btnOtherClienteDrawing.setVisibility(View.VISIBLE);
                    }else {
                        btnOtherClienteDrawing.setText(String.format(getString(R.string.client_stop_drawing), clientSupport.getName()));
                        (new Thread(){
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(3000);
                                }catch(Exception e){}
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnOtherClienteDrawing.setVisibility(supportIsDrawing?View.VISIBLE:View.GONE);
                                    }
                                });
                            }
                        }).start();
                    }
                }
                else if (intent.hasExtra(SocketService.OUT_CLEAR_DRAW_PARAM)){
                    imageView.setImageDrawable(null);
                }
                else if (intent.hasExtra(SocketService.VIDEO_CAN_CONNECT)) {
                    sendWEBRTCMessage(SocketService.VIDEO_CREATEOFFER, "");
                }
                else if (intent.hasExtra(SocketService.VIDEO_OFFER)){
                    try {
                        JSONObject obj = new JSONObject(intent.getStringExtra(SocketService.VIDEO_OFFER));
                        SessionDescription sdp = new SessionDescription(SessionDescription.Type.OFFER,
                                obj.getString(SDP));
                        peerConnection.setRemoteDescription(mySdpObserver, sdp);
                        peerConnection.createAnswer(mySdpObserver, new MediaConstraints());
                    }catch (Exception e){e.printStackTrace();}
                }
                else if (intent.hasExtra(SocketService.VIDEO_CANDIDATE)){
                    try {
                        JSONObject obj = new JSONObject(intent.getStringExtra(SocketService.VIDEO_CANDIDATE));
                        peerConnection.addIceCandidate(
                                new IceCandidate(obj.getString(SDP_MID),
                                                 obj.getInt(SDP_M_LINE_INDEX),
                                                 obj.getString(SDP)));
                    }catch (Exception e){e.printStackTrace();}
                }
            }else if (intent.getAction().equals(SocketService.UPDATE_CLIENT_TASK)) {
                int index = ClientsController.getInstance().getClientList().indexOf(clientSupport);
                if (index <0 || ClientsController.getInstance().getClientList().get(index).isShow())
                    finish();
            }
        }
    }

    private class MySdpObserver implements SdpObserver {
        @Override
        public void onCreateSuccess(SessionDescription sessionDescription) {
            peerConnection.setLocalDescription(mySdpObserver, sessionDescription);
            try {
                JSONObject obj = new JSONObject();
                obj.put(SDP, sessionDescription.description);
                sendWEBRTCMessage(SocketService.VIDEO_ANSWER, obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSetSuccess() {}

        @Override
        public void onCreateFailure(String s) {}

        @Override
        public void onSetFailure(String s) {}
    }

    private class PeerConnectionObserver implements PeerConnection.Observer{

        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {}

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {}

        @Override
        public void onIceConnectionReceivingChange(boolean b) {}

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {}

        @Override
        public void onIceCandidate(IceCandidate iceCandidate) {
            try {
                JSONObject obj = new JSONObject();
                obj.put(SDP_MID, iceCandidate.sdpMid);
                obj.put(SDP_M_LINE_INDEX, iceCandidate.sdpMLineIndex);
                obj.put(SDP, iceCandidate.sdp);
                sendWEBRTCMessage(SocketService.VIDEO_CANDIDATE, obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onAddStream(MediaStream mediaStream) {}

        @Override
        public void onRemoveStream(MediaStream mediaStream) {}

        @Override
        public void onDataChannel(DataChannel dataChannel) {}

        @Override
        public void onRenegotiationNeeded() {}
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (imageView.getDrawable()==null)
            return;

        float valueY = event.values[1] * 7;
        float valueX = event.values[0] * 7;

        imageView.setX(imageView.getX()+valueY);
        imageView.setY(imageView.getY()+valueX);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
