package br.edu.ifspsaocarlos.sdm.toolview;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import br.edu.ifspsaocarlos.sdm.toolview.controller.ClientsController;
import br.edu.ifspsaocarlos.sdm.toolview.model.Client;
import br.edu.ifspsaocarlos.sdm.toolview.model.type.ServerMessageType;
import br.edu.ifspsaocarlos.sdm.toolview.service.SocketService;

public class CallActivity extends AppCompatActivity {

    public static final String CLIENT_PARAM = "CLIENT_PARAM";

    public static final int REQUEST_CAMERA = 0;
    public static final int REQUEST_AUDIO = 1;

    private DataUpdateReceiver dataUpdateReceiver;

    private TextView tvName;
    private TextView tvCalling;
    private ImageButton btnCall;
    private Button btnCancel;

    private Client clientSupport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        clientSupport = (Client) getIntent().getSerializableExtra(CLIENT_PARAM);

        btnCall = findViewById(R.id.btnCall);
        btnCancel = findViewById(R.id.btnCancel);
        tvCalling = findViewById(R.id.tvCalling);
        tvName = findViewById(R.id.tvName);

        if (clientSupport==null)
            finish();

        tvName.setText(clientSupport.getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilterNewMessage = new IntentFilter(SocketService.CALL_TASK);
        IntentFilter intentFilterNewMessage2 = new IntentFilter(SocketService.UPDATE_CLIENT_TASK);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage2);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null)
            unregisterReceiver(dataUpdateReceiver);
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SocketService.CALL_TASK)) {
                if (intent.hasExtra(SocketService.OUT_CALL_STATUS_PARAM)){
                    ServerMessageType serverMessageType = (ServerMessageType) intent.getSerializableExtra(SocketService.OUT_CALL_STATUS_PARAM);
                    switch (serverMessageType){
                        case USER_ACCEPT:
                            Intent videoIntent = new Intent(CallActivity.this, VideoCustomerActivity.class);
                            videoIntent.putExtra(VideoCustomerActivity.CLIENT_PARAM, clientSupport);
                            startActivity(videoIntent);
                            finish();
                            break;
                        case USER_DENY:
                            finish();
                            break;
                        default:
                            break;
                    }
                }
            }else if (intent.getAction().equals(SocketService.UPDATE_CLIENT_TASK)) {
                int index = ClientsController.getInstance().getClientList().indexOf(clientSupport);
                if (index <0) {
                    finish();
                    return;
                }
                clientSupport = ClientsController.getInstance().getClientList().get(index);
                tvName.setText(clientSupport.getName());
            }
        }
    }

    public void call(View view){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_AUDIO);
            else
                call();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                        checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                    call();
                else
                    requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_AUDIO);
            }
        }else if(requestCode == REQUEST_AUDIO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                        checkSelfPermission(Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED)
                    call();
                else
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            }
        }
    }

    private void call(){
        tvCalling.setVisibility(View.VISIBLE);
        btnCall.setEnabled(false);
        btnCall.setBackgroundColor(getResources().getColor(R.color.colorDisable));

        callClient();
    }

    public void cancel(View view){
        btnCancel.setEnabled(false);
        btnCancel.setBackgroundColor(getResources().getColor(R.color.colorDisable));

        cancelCall();
        finish();
    }

    private void callClient(){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_CALL_CLIENT_PARAM, clientSupport);
        startService(sendSocketServiceIntent);
    }

    private void cancelCall(){
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_CANCEL_CALL_CLIENT_PARAM, clientSupport);
        startService(sendSocketServiceIntent);
    }

}
