package br.edu.ifspsaocarlos.sdm.toolview;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import br.edu.ifspsaocarlos.sdm.toolview.service.SocketService;

public class MainActivity extends Activity {

    private Intent socketServiceIntent;

    private DataUpdateReceiver dataUpdateReceiver;

    private EditText etName;

    private ProgressBar pbLoading;

    private Button btnSendName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = findViewById(R.id.etName);
        pbLoading = findViewById(R.id.pbLoading);
        btnSendName = findViewById(R.id.btnSendName);

        socketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        startService(socketServiceIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilterNewMessage = new IntentFilter(SocketService.CONNECTION_STATUS_TASK);
        registerReceiver(dataUpdateReceiver, intentFilterNewMessage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null)
            unregisterReceiver(dataUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        stopService(socketServiceIntent);
        super.onDestroy();
    }

    public void sendName(View view){
        if (etName.getText().toString().length()<=0)
            return;
        Intent sendSocketServiceIntent = new Intent(getApplicationContext(), SocketService.class);
        sendSocketServiceIntent.putExtra(SocketService.IN_NAME_PARAM, etName.getText().toString());
        startService(sendSocketServiceIntent);
        btnSendName.setEnabled(false);
        Intent intentClients = new Intent(MainActivity.this, ClientsActivity.class);
        startActivity(intentClients);
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(SocketService.OUT_CONNECTION_STATUS_PARAM) &&
                    intent.getBooleanExtra(SocketService.OUT_CONNECTION_STATUS_PARAM, false)) {
                pbLoading.setVisibility(View.GONE);
                btnSendName.setEnabled(true);
                btnSendName.setText(R.string.msg_send);
            }
        }
    }
}