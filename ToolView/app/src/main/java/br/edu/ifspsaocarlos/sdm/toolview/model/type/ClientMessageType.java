package br.edu.ifspsaocarlos.sdm.toolview.model.type;

public enum ClientMessageType {
	CHANGE_NAME, CALL_USER, CANCEL_CALL_USER, ACCEPT_CALL, DENY_CALL, EXIT_CALL, EXIT, DRAWING, DREW,
	CLEAR_DRAW, CAN_CONNECT, OFFER, CANDIDATE, ANSWER, CREATEOFFER;
}
